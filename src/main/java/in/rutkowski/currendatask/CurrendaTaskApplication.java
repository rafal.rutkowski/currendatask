package in.rutkowski.currendatask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CurrendaTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(CurrendaTaskApplication.class, args);
    }

}
