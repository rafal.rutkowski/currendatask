package in.rutkowski.currendatask.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {
    public final static String DATE_FORMAT = "yyyy-MM-dd";
    public final static SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat(DATE_FORMAT);

    public static boolean isDateBetweenInclusive(Date date, Date from, Date to){
        return from.compareTo(date) * date.compareTo(to) >= 0;
    }
}
