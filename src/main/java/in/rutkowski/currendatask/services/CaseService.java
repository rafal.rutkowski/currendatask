package in.rutkowski.currendatask.services;

import in.rutkowski.currendatask.model.data.AddressEntity;
import in.rutkowski.currendatask.model.repository.CaseEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class CaseService {
    @Autowired
    CaseEntityRepository caseEntityRepository;

    public Map<String, Integer> getCountRecordsGroupByCaseState(Date dateFrom, Date dateTo, String caseType){
        return caseEntityRepository.countRecordsGroupByCaseState(dateFrom, dateTo, caseType);
    }

    public List<AddressEntity> getActiveAddresses(String caseType){
        return caseEntityRepository.getActiveAddresses(caseType);
    }
}
