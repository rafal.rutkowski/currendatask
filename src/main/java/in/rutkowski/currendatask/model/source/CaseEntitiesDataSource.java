package in.rutkowski.currendatask.model.source;

import com.fasterxml.jackson.databind.ObjectMapper;
import in.rutkowski.currendatask.model.data.CaseEntity;
import in.rutkowski.currendatask.model.data.PartyEntity;
import in.rutkowski.currendatask.utils.DateUtils;
import jakarta.annotation.PostConstruct;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
@Scope("singleton")
public class CaseEntitiesDataSource {
    @Value("${currenda_task.partyEntityFilePath}")
    private String partyEntityFilePath;
    @Value("${currenda_task.caseEntityFilePath}")
    private String caseEntityFilePath;
    @Getter
    private List<CaseEntity> caseEntities;

    @PostConstruct
    public void initialize() throws IOException {
        File caseEntityFile = new File(caseEntityFilePath);
        File partyEntityFile = new File(partyEntityFilePath);

        ObjectMapper objectMapper = new ObjectMapper();

        configureObjectMapper(objectMapper);

        List<CaseEntity> caseEntities = objectMapper.readValue(caseEntityFile, objectMapper.getTypeFactory().constructCollectionType(List.class, CaseEntity.class));
        List<PartyEntity> partyEntities = objectMapper.readValue(partyEntityFile, objectMapper.getTypeFactory().constructCollectionType(List.class, PartyEntity.class));

        caseEntities.forEach( caseEntity -> caseEntity.setPartyEntities(partyEntities.stream()
                .filter( partyEntity -> Objects.equals(partyEntity.getCaseId(), caseEntity.getCaseId()))
                .collect(Collectors.toUnmodifiableList())));

        this.caseEntities = Collections.unmodifiableList(caseEntities);
    }

    private void configureObjectMapper(ObjectMapper objectMapper) {
        objectMapper.setDateFormat(DateUtils.SIMPLE_DATE_FORMAT);
    }
}
