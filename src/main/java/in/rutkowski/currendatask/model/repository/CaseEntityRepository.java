package in.rutkowski.currendatask.model.repository;

import in.rutkowski.currendatask.model.data.AddressEntity;
import in.rutkowski.currendatask.model.data.CaseEntity;
import in.rutkowski.currendatask.model.data.PartyEntity;
import in.rutkowski.currendatask.model.source.CaseEntitiesDataSource;
import in.rutkowski.currendatask.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

@Repository
public class CaseEntityRepository{
    @Autowired
    CaseEntitiesDataSource caseEntitiesDataSource;

    private Set<String> _allCaseTypes;

    public Map<String, Integer> countRecordsGroupByCaseState(Date dateOfEntryFrom, Date dateOfEntryTo, String caseType){
        List<CaseEntity> caseEntities = caseEntitiesDataSource.getCaseEntities();
        Map<String, Integer> caseStateIntegerMap = caseEntities.stream()
                .filter(caseEntity -> Objects.equals(caseEntity.getCaseType(), caseType))
                .filter(caseEntity -> DateUtils.isDateBetweenInclusive(caseEntity.getDateOfEntry(), dateOfEntryFrom, dateOfEntryTo))
                .collect(Collectors.groupingBy(CaseEntity::getCaseState))
                .entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().size()));

        return caseStateIntegerMap;
    }

    public List<AddressEntity> getActiveAddresses(String caseType){
        List<CaseEntity> caseEntities = caseEntitiesDataSource.getCaseEntities();

        List<AddressEntity> addressEntities = caseEntities.stream()
                .filter(caseEntity -> Objects.equals(caseEntity.getCaseType(), caseType))
                .map(CaseEntity::getPartyEntities)
                .flatMap(Collection::stream)
                .filter(PartyEntity::getActive)
                .map(PartyEntity::getAddresses)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        return addressEntities;
    }

    public Set<String> getAllCaseTypes(){
        if(_allCaseTypes == null){
            _allCaseTypes = caseEntitiesDataSource.getCaseEntities().stream()
                    .map(CaseEntity::getCaseType)
                    .collect(Collectors.toSet());
        }

        return _allCaseTypes;
    }
}
