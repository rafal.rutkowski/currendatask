package in.rutkowski.currendatask.model.data;

import lombok.Data;

@Data
public class AddressEntity {
    Long id;
    String city;
    String postalCode;
    String street;
}
