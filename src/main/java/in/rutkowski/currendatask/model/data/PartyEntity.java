package in.rutkowski.currendatask.model.data;

import lombok.Data;

import java.util.List;
@Data
public class PartyEntity {
    Long caseId;
    String partyType;
    String name;
    Boolean active;
    List<AddressEntity> addresses;
}
