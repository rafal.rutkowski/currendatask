package in.rutkowski.currendatask.model.data;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class CaseEntity {
    Long caseId;
    String caseNumber;
    String caseType;
    String caseState;
    Date dateOfEntry;
    List<PartyEntity> partyEntities;
}
