package in.rutkowski.currendatask.model.request;

import in.rutkowski.currendatask.model.validation.CaseTypeConstraint;
import in.rutkowski.currendatask.utils.DateUtils;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class CountRecordsGroupByCaseStateRequest {
    @NotNull
    @DateTimeFormat(pattern = DateUtils.DATE_FORMAT)
    Date dateFrom;

    @NotNull
    @DateTimeFormat(pattern = DateUtils.DATE_FORMAT)
    Date dateTo;

    @NotNull
    @NotBlank
    @CaseTypeConstraint
    String caseType;
}
