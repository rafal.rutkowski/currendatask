package in.rutkowski.currendatask.model.validation;

import in.rutkowski.currendatask.model.repository.CaseEntityRepository;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.beans.factory.annotation.Autowired;

public class CaseTypeValidator implements ConstraintValidator<CaseTypeConstraint, String> {
    @Autowired
    CaseEntityRepository caseEntityRepository;
    @Override
    public void initialize(CaseTypeConstraint constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if (s == null){
            return true;
        }

        return caseEntityRepository.getAllCaseTypes().contains(s);
    }
}
