package in.rutkowski.currendatask.model.validation;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = CaseTypeValidator.class)
@Target({ElementType.PARAMETER, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface CaseTypeConstraint {
    String message() default "Invalid case type";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
