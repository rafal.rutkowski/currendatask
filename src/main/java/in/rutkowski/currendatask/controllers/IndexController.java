package in.rutkowski.currendatask.controllers;

import in.rutkowski.currendatask.model.repository.CaseEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class IndexController {
    @Autowired
    CaseEntityRepository caseEntityRepository;


    @RequestMapping(method = RequestMethod.GET, value = "/")
    public String index(Model model){
        model.addAttribute("allCaseTypes", caseEntityRepository.getAllCaseTypes());

        return "index";
    }
}
