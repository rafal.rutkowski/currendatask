package in.rutkowski.currendatask.controllers;

import in.rutkowski.currendatask.model.data.AddressEntity;
import in.rutkowski.currendatask.model.request.CountRecordsGroupByCaseStateRequest;
import in.rutkowski.currendatask.model.validation.CaseTypeConstraint;
import in.rutkowski.currendatask.services.CaseService;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.ServletWebRequest;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/case")
@Validated
public class CaseRestController {
    @Autowired
    CaseService caseService;
    @GetMapping("count-records-group-by-case-state")
    public @ResponseBody ResponseEntity<Map<String, Integer>> getCountRecordsGroupByCaseState(@Valid CountRecordsGroupByCaseStateRequest countRecordsGroupByCaseStateRequest){
        return ResponseEntity.of(Optional.ofNullable(caseService.getCountRecordsGroupByCaseState(countRecordsGroupByCaseStateRequest.getDateFrom(), countRecordsGroupByCaseStateRequest.getDateTo(), countRecordsGroupByCaseStateRequest.getCaseType())));
    }

    @GetMapping("get-active-addresses")
    public @ResponseBody ResponseEntity<List<AddressEntity>> getActiveAddresses(@RequestParam @NotNull @NotBlank @CaseTypeConstraint String caseType){
        return ResponseEntity.of(Optional.ofNullable(caseService.getActiveAddresses(caseType)));
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public void handleConstraintViolationException(ConstraintViolationException exception, ServletWebRequest webRequest) throws IOException {
        webRequest.getResponse().sendError(HttpStatus.BAD_REQUEST.value(), exception.getMessage());
    }
}
