package in.rutkowski.currendatask;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.Map;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CaseRestControllerTest {
    private final static String CASE_CONTROLLER_MAPPING = "/case";
    @Autowired
    protected TestRestTemplate testRestTemplate;

    @LocalServerPort
    private int port;


    @Test
    public void performCountRecordsGroupByCaseStateValidateIncorrectCaseTypeTest(){
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(getCountRecordsGroupByCaseStateURLEndpoint())
                .queryParam("dateFrom", "2022-11-18")
                .queryParam("dateTo", "2022-11-28")
                .queryParam("caseType", "NotExisting");

        ResponseEntity<Map> entity = testRestTemplate.getForEntity(
                builder.build().encode().toUri(),
                Map.class
        );

        Assertions.assertEquals(HttpStatusCode.valueOf(400), entity.getStatusCode());
    }
    @Test
    public void performCountRecordsGroupByCaseStateValidateEmptyDateTest(){
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(getCountRecordsGroupByCaseStateURLEndpoint())
                .queryParam("dateFrom", "")
                .queryParam("dateTo", "2022-11-28")
                .queryParam("caseType", "P");

        ResponseEntity<Map> entity = testRestTemplate.getForEntity(
                builder.build().encode().toUri(),
                Map.class
        );

        Assertions.assertEquals(HttpStatusCode.valueOf(400), entity.getStatusCode());
    }

    @Test
    public void performCountRecordsGroupByCaseStateValidCaseTypeTest(){
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(getCountRecordsGroupByCaseStateURLEndpoint())
                .queryParam("dateFrom", "2022-11-18")
                .queryParam("dateTo", "2022-11-28")
                .queryParam("caseType", "P");

        ResponseEntity<Map> entity = testRestTemplate.getForEntity(
                builder.build().encode().toUri(),
                Map.class
        );

        Assertions.assertEquals(HttpStatusCode.valueOf(200), entity.getStatusCode());
    }

    @Test
    public void performGetActiveAddressesValidCaseTypeTest(){
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(getGetActiveAddressesURLEndpoint())
                .queryParam("caseType", "P");

        ResponseEntity<List> entity = testRestTemplate.getForEntity(
                builder.build().encode().toUri(),
                List.class
        );

        Assertions.assertEquals(HttpStatusCode.valueOf(200), entity.getStatusCode());
    }

    @Test
    public void performGetActiveAddressesInvalidCaseTypeTest(){
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(getGetActiveAddressesURLEndpoint())
                .queryParam("caseType", "NotExistingCaseType");

        ResponseEntity entity = testRestTemplate.getForEntity(
                builder.build().encode().toUri(),
                Map.class
                );

        Assertions.assertEquals(HttpStatusCode.valueOf(400), entity.getStatusCode());
    }

    @Test
    public void performGetActiveAddressesInvalidBlankCaseTypeTest(){
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(getGetActiveAddressesURLEndpoint())
                .queryParam("caseType", " ");

        ResponseEntity entity = testRestTemplate.getForEntity(
                builder.build().encode().toUri(),
                Map.class
        );

        Assertions.assertEquals(HttpStatusCode.valueOf(400), entity.getStatusCode());
    }

    private String getApplicationUrl(){
        return "http://localhost:" + port;
    }

    private String getCountRecordsGroupByCaseStateURLEndpoint(){
        return getApplicationUrl() + CASE_CONTROLLER_MAPPING + "/count-records-group-by-case-state";
    }
    private String getGetActiveAddressesURLEndpoint(){
        return getApplicationUrl() + CASE_CONTROLLER_MAPPING + "/get-active-addresses";
    }
}
