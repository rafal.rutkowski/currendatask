package in.rutkowski.currendatask;

import in.rutkowski.currendatask.model.data.AddressEntity;
import in.rutkowski.currendatask.model.repository.CaseEntityRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@SpringBootTest
public class CaseRepositoryTest {
    @Autowired
    CaseEntityRepository caseEntityRepository;
    final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");

    @Test
    void performCountRecordsGroupByCaseStateTest() throws ParseException {
        Date dateFrom = dateFormatter.parse("2022-11-18");
        Date dateTo = dateFormatter.parse("2022-11-28");
        Map<String, Integer> countRecordsGroupByCaseState = caseEntityRepository.countRecordsGroupByCaseState(dateFrom, dateTo, "P");

        Assertions.assertNotNull(countRecordsGroupByCaseState);
        Assertions.assertEquals(1 , countRecordsGroupByCaseState.keySet().size());
        Assertions.assertEquals(2, countRecordsGroupByCaseState.get("OPEN"));
    }
    @Test
    void performFindActiveAddressesTest(){
        List<AddressEntity> activeAddresses = caseEntityRepository.getActiveAddresses("K");

        Assertions.assertNotNull(activeAddresses);
        Assertions.assertEquals( 3, activeAddresses.size());
    }
}
